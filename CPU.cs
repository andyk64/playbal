﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PlayBAL
{
    public static class CPU
    {
        public static Encoding EBCDIC => Encoding.GetEncoding("IBM037");

        // CPU / INPUT / OUTPUT
        public static List<byte> Bytecode;

        public static List<byte> InputData;
        public static int InputHead = 0;

        public static string OutputFrame { get; set; } = string.Empty;

        public static string OutputText = string.Empty;

        static CPU()
        {
            Reset();
        }

        public static void Reset()
        {
            Registers = new int[16] { 0, 0, 0, 0, 0, 0, -56, 0, 0, 0, 0, 0, 0, 0, -1, 0 };
            InputHead = 0;
            Head = 0;
            Finished = false;
            ConditionCode = 15;

            OutputText = string.Empty;
        }

        public static void Load(List<byte> bytes, string input)
        {
            Bytecode = bytes;
            InputData = Encoding.Convert(Encoding.ASCII, Encoding.GetEncoding("IBM037"), Encoding.GetEncoding("UTF-8").GetBytes(input)).ToList();
        }

        public static void Step()
        {
            if (Head < 0 || Head >= Bytecode.Count) return;

            // FOR DEBUG MESSAGES UNCOMMENT:
            PrintDebug();

            var operation = Operation.Operations[Bytecode[Head]];
            if (operation == null) throw new Exception("OpCode " + Bytecode[Head] + " does not exist");

            operation.Invoke();

            // move head forward or check if finished
            if (Head == -1)
            {
                Finished = true;
                return; // unnecessary return
            }
        }

        public static void SetCC(int intValue)
        {
            if (intValue > 0)
            {
                ConditionCode = 8;
            }
            else if (intValue == 0)
            {
                ConditionCode = 4;
            }
            else
            {
                ConditionCode = 2;
            }
        }

        public static void SetCC(uint zero, uint one, uint two, uint three)
        {
            ConditionCode = (byte)(((zero > 0 ? 1 : 0) << 3) + ((one > 0 ? 1 : 0) << 2) + ((two > 0 ? 1 : 0) << 1) + ((three > 0 ? 1 : 0) << 0));
        }

        public static byte[] ReadInput(int length)
        {
            List<byte> bytes = new();

            for (int end = InputHead + length; InputHead < end; ++InputHead)
            {
                bytes.Add(InputData[InputHead]);

                if (InputData[InputHead] == 0x25)
                {
                    InputHead++;
                    break;
                }
            }

            for (int i = bytes.Count; i < length; ++i) bytes.Add(0x00);

            return bytes.ToArray();
        }

        public static void WriteOutput(string output)
        {
            OutputText += output;
        }

        public static byte[] ReadMemory(int address, int length)
        {
            return Bytecode.GetRange(address, length).Reverse<byte>().ToArray();
        }

        public static void WriteMemory(int address, byte[] bytes)
        {
            for (int i = 0; i < bytes.Length; ++i)
            {
                if (address + i < 0 || address + i >= Bytecode.Count)
                {
                    // throw exception
                    System.Diagnostics.Debug.WriteLine("WRITING MEMORY OUT OF BOUNDS ERROR");
                    return;
                }

                Bytecode[address + i] = bytes[i];
            }
        }

        public static void PrintDebug()
        {
            System.Diagnostics.Debug.WriteLine(Head + ": " + Bytecode[Head] + ": " + Operation.Mnemonics.First(x => x.Value == Bytecode[Head]).Key);
            System.Diagnostics.Debug.WriteLine("I: " + I);
            System.Diagnostics.Debug.WriteLine("L: " + L);
            System.Diagnostics.Debug.WriteLine("R1: " + R1);
            System.Diagnostics.Debug.WriteLine("R2: " + R2);
            System.Diagnostics.Debug.WriteLine("X1: " + X1);
            System.Diagnostics.Debug.WriteLine("B1: " + B1);
            System.Diagnostics.Debug.WriteLine("D1: " + D1);
            System.Diagnostics.Debug.WriteLine("B2: " + B2);
            System.Diagnostics.Debug.WriteLine("D2: " + D2);
            System.Diagnostics.Debug.WriteLine("ADDR1: " + DXB1);
            System.Diagnostics.Debug.WriteLine("ADDR2: " + DB2);
        }

        // CPU registers
        public static Int32[] Registers { get; set; }
        public static Int32 Head { get; set; }
        public static byte ConditionCode = 15;
        public static bool Finished = false;
        // state members
        public static int I => Bytecode[Head + 1];
        public static int L => I;

        public static int R1 => Bytecode[Head + 1] >> 4;
        public static int L1 => R1;
        public static int R2 => Bytecode[Head + 1] & 0x0F;
        public static int L2 => R2;

        public static int B1 => Bytecode[Head + 2] >> 4;
        public static int X1 => Bytecode[Head + 1] & 0x0F;
        public static int D1 => ((int)(Bytecode[Head + 2] & 0x0F) << 8) + Bytecode[Head + 3];
        public static int DB1 => D1 + Registers[B1];
        public static int DXB1 => D1 + Registers[X1] + Registers[B1];

        public static int B2 => Bytecode[Head + 4] >> 4;
        public static int D2 => ((int)(Bytecode[Head + 4] & 0x0F) << 8) + Bytecode[Head + 5];
        public static int DB2 => D2 + Registers[B2];
    }
}
