﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace PlayBAL
{
    public static class Operation
    {
        public static Dictionary<string, byte> Mnemonics = new();
        public static Action[] Operations = new Action[0xFF];
        public static Dictionary<byte, OperationType> OperationTypes = new();

        public enum OperationType
        {
            I,
            RR,
            RS,
            RSI,
            RX,
            SI,
            SS,
            XX,
        }

        public static OperationType GetType(byte bytecode)
        {
            if (OperationTypes.ContainsKey(bytecode)) return OperationTypes[bytecode];

            return (bytecode >> 6) switch
            {
                0 => OperationType.I,
                1 => OperationType.RX,
                2 => OperationType.RS,
                3 => OperationType.SS,
                _ => OperationType.RR,
            };
        }
        public static int GetLength(byte opcode)
        {
            switch (Operation.GetType(opcode))
            {
                case Operation.OperationType.RR:
                    return 2;
                case Operation.OperationType.RX:
                    return 4;
                case Operation.OperationType.RS:
                    return 4;
                case Operation.OperationType.XX:
                    return 6;
                case Operation.OperationType.SI:
                    return 4;
                case Operation.OperationType.SS:
                    return 6;
                default:
                    break;
            }

            return (opcode >> 6) switch
            {
                0 => 2,
                1 => 4,
                2 => 4,
                3 => 6,
                _ => 4,
            };
        }

        public static void ONI()
        {
            System.Diagnostics.Debug.WriteLine("ONI (OPERATION NOT IMPLEMENTED): " + Operation.Mnemonics.First(x => x.Value == CPU.Bytecode[CPU.Head]).Key);
        }

        public static void XIO()
        {
            if (CPU.R1 == 0) // XREAD
            {
                XREAD();
            }
            else if (CPU.R1 == 2) // XPRNT
            {
                XPRNT();
            }

            CPU.Head += 6;
        }


        // CUSTOM OPERATIONS
        // 0x52
        public static void XDECO()
        {
            var bytes = CPU.EBCDIC.GetBytes(string.Format("{0,-12}", CPU.Registers[CPU.R1]));

            CPU.WriteMemory(CPU.DXB1, bytes);

            CPU.Head += 4;

        }

        // 0x53
        public static void XDECI()
        {
            bool negative = false;
            int scannedInt = 0;
            int scannedChars = 0;
            int pos = 0;
            for (int i = CPU.DXB1; i < CPU.Bytecode.Count; ++i)
            {                
                switch (CPU.Bytecode[i])
                {
                    case 0x2B: // '+'
                        if (scannedChars == 0)
                        {
                            negative = false;
                        }
                        else
                        {
                            pos = i;
                            scannedInt = (negative ? -1 : 1) * scannedInt;
                            CPU.SetCC(scannedInt);
                            i = CPU.Bytecode.Count; // break out of for loop
                        }
                        break;
                    case 0x40:
                        if (scannedChars == 0)
                        {
                            continue;
                        }
                        else
                        {
                            pos = i;
                            scannedInt = (negative ? -1 : 1) * scannedInt;
                            CPU.SetCC(scannedInt);
                            i = CPU.Bytecode.Count; // break out of for loop
                        }
                        break;
                    case 0x60: // '-'
                        if (scannedChars == 0)
                        {
                            negative = true;
                        }
                        else
                        {
                            pos = i;
                            scannedInt = (negative ? -1 : 1) * scannedInt;
                            CPU.SetCC(scannedInt);
                            i = CPU.Bytecode.Count; // break out of for loop
                        }
                        break;
                    case 0xF0:
                    case 0xF1:
                    case 0xF2:
                    case 0xF3:
                    case 0xF4:
                    case 0xF5:
                    case 0xF6:
                    case 0xF7:
                    case 0xF8:
                    case 0xF9:
                        scannedInt = (scannedInt * 10) + (CPU.Bytecode[i] & 0x0F);

                        if (++scannedChars == 9)
                        {
                            pos = i;
                            scannedInt = (negative ? -1 : 1) * scannedInt;
                            CPU.SetCC(scannedInt);
                            i = CPU.Bytecode.Count; // break out of for loop
                        }
                        break;
                    default:
                        if (scannedChars == 0)
                        {
                            pos = i;
                            scannedInt = CPU.Registers[CPU.R1];
                            CPU.SetCC(0, 0, 0, 1);
                            i = CPU.Bytecode.Count; // break out of for loop
                        }
                        else
                        {
                            pos = i;
                            scannedInt = (negative ? -1 : 1) * scannedInt;
                            CPU.SetCC(scannedInt);
                            i = CPU.Bytecode.Count; // break out of for loop
                        }
                        break;
                }
            }

            CPU.Registers[1] = pos;
            CPU.Registers[CPU.R1] = scannedInt;

            CPU.Head += 4;
        }

        // 0xE0
        public static void XREAD()
        {
            var input = CPU.ReadInput(CPU.D2);
            CPU.WriteMemory(CPU.DXB1, input);

            // System.Diagnostics.Debug.WriteLine(Encoding.GetEncoding("IBM037").GetString(input));

            if (CPU.InputHead >= CPU.InputData.Count)
            {
                CPU.SetCC(0, 1, 0, 0);
            }
            else
            {
                CPU.SetCC(1, 0, 0, 0);
            }
        }

        // 0xE0
        public static void XPRNT()
        {
            string str = CPU.EBCDIC.GetString(CPU.ReadMemory(CPU.DXB1, CPU.D2).Reverse().ToArray());
            CPU.WriteOutput(Regex.Replace(str, "[^a-zA-Z0-9_. +-]+", "", RegexOptions.Compiled) + Environment.NewLine);
        }

        // 0xE1
        public static void XDUMP()
        {
            System.Diagnostics.Debug.WriteLine("XDUMP INSTRUCTION NOT IMPLEMENTED");
        }


        // STANDARD OPERATIONS
        // 0x04
        public static void SPM()
        {
            System.Diagnostics.Debug.WriteLine("SPM INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x05
        public static void BALR()
        {
            System.Diagnostics.Debug.WriteLine("BALR INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x06
        public static void BCTR()
        {
            System.Diagnostics.Debug.WriteLine("BCTR INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x07
        public static void BCR()
        {
            if (CPU.R2 == 0) return;

            if ((CPU.ConditionCode & CPU.R1) > 0 || CPU.R1 == 15) // UNSURE IF 15 COMPARISON IS NECESSARY HERE, CONDITION CODE SHOULD NEVER BE 0 I THINK
            {
                CPU.Head = CPU.Registers[CPU.R2];
            }
        }

        // 0x07
        public static void NOPR()
        {
            System.Diagnostics.Debug.WriteLine("NOPR INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x07
        public static void BR()
        {
            System.Diagnostics.Debug.WriteLine("BR INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x08
        public static void SSK()
        {
            System.Diagnostics.Debug.WriteLine("SSK INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x09
        public static void ISK()
        {
            System.Diagnostics.Debug.WriteLine("ISK INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x0A
        public static void SVC()
        {
            System.Diagnostics.Debug.WriteLine("SVC INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x0D
        public static void BASR()
        {
            System.Diagnostics.Debug.WriteLine("BASR INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x10
        public static void LPR()
        {
            System.Diagnostics.Debug.WriteLine("LPR INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x11
        public static void LNR()
        {
            System.Diagnostics.Debug.WriteLine("LNR INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x12
        public static void LTR()
        {
            System.Diagnostics.Debug.WriteLine("LTR INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x13
        public static void LCR()
        {
            System.Diagnostics.Debug.WriteLine("LCR INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x14
        public static void NR()
        {
            System.Diagnostics.Debug.WriteLine("NR INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x15
        public static void CLR()
        {
            System.Diagnostics.Debug.WriteLine("CLR INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x16
        public static void OR()
        {
            System.Diagnostics.Debug.WriteLine("OR INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x17
        public static void XR()
        {
            System.Diagnostics.Debug.WriteLine("XR INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x18
        public static void LR()
        {
            CPU.Registers[CPU.R1] = CPU.Registers[CPU.R2];

            CPU.Head += 2;
        }

        // 0x19
        public static void CR()
        {
            if (CPU.Registers[CPU.R1] == CPU.Registers[CPU.R2])
            {
                CPU.SetCC(1, 0, 0, 0);
            }
            else if (CPU.Registers[CPU.R1] < CPU.Registers[CPU.R2])
            {
                CPU.SetCC(0, 1, 0, 0);
            }
            else
            {
                CPU.SetCC(0, 0, 1, 0);
            }

            CPU.Head += 2;
        }

        // 0x1A
        public static void AR()
        {
            CPU.Registers[CPU.R1] += CPU.Registers[CPU.R2];

            CPU.Head += 2;
        }

        // 0x1B
        public static void SR()
        {
            CPU.Registers[CPU.R1] -= CPU.Registers[CPU.R2];

            CPU.Head += 2;
        }

        // 0x1C
        public static void MR()
        {
            System.Diagnostics.Debug.WriteLine("MR INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x1D
        public static void DR()
        {
            System.Diagnostics.Debug.WriteLine("DR INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x1E
        public static void ALR()
        {
            System.Diagnostics.Debug.WriteLine("ALR INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x1F
        public static void SLR()
        {
            System.Diagnostics.Debug.WriteLine("SLR INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x20
        public static void LPDR()
        {
            System.Diagnostics.Debug.WriteLine("LPDR INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x21
        public static void LNDR()
        {
            System.Diagnostics.Debug.WriteLine("LNDR INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x22
        public static void LTDR()
        {
            System.Diagnostics.Debug.WriteLine("LTDR INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x23
        public static void LCDR()
        {
            System.Diagnostics.Debug.WriteLine("LCDR INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x24
        public static void HDR()
        {
            System.Diagnostics.Debug.WriteLine("HDR INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x25
        public static void LRDR()
        {
            System.Diagnostics.Debug.WriteLine("LRDR INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x26
        public static void MXR()
        {
            System.Diagnostics.Debug.WriteLine("MXR INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x27
        public static void MXDR()
        {
            System.Diagnostics.Debug.WriteLine("MXDR INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x28
        public static void LDR()
        {
            System.Diagnostics.Debug.WriteLine("LDR INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x29
        public static void CDR()
        {
            System.Diagnostics.Debug.WriteLine("CDR INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x2A
        public static void ADR()
        {
            System.Diagnostics.Debug.WriteLine("ADR INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x2B
        public static void SDR()
        {
            System.Diagnostics.Debug.WriteLine("SDR INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x2C
        public static void MDR()
        {
            System.Diagnostics.Debug.WriteLine("MDR INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x2D
        public static void DDR()
        {
            System.Diagnostics.Debug.WriteLine("DDR INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x2E
        public static void AWR()
        {
            System.Diagnostics.Debug.WriteLine("AWR INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x2F
        public static void SWR()
        {
            System.Diagnostics.Debug.WriteLine("SWR INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x30
        public static void LPER()
        {
            System.Diagnostics.Debug.WriteLine("LPER INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x31
        public static void LNER()
        {
            System.Diagnostics.Debug.WriteLine("LNER INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x32
        public static void LTER()
        {
            System.Diagnostics.Debug.WriteLine("LTER INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x33
        public static void LCER()
        {
            System.Diagnostics.Debug.WriteLine("LCER INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x34
        public static void HER()
        {
            System.Diagnostics.Debug.WriteLine("HER INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x35
        public static void LRER()
        {
            System.Diagnostics.Debug.WriteLine("LRER INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x36
        public static void AXR()
        {
            System.Diagnostics.Debug.WriteLine("AXR INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x37
        public static void SXR()
        {
            System.Diagnostics.Debug.WriteLine("SXR INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x38
        public static void LER()
        {
            System.Diagnostics.Debug.WriteLine("LER INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x39
        public static void CER()
        {
            System.Diagnostics.Debug.WriteLine("CER INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x3A
        public static void AER()
        {
            System.Diagnostics.Debug.WriteLine("AER INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x3B
        public static void SER()
        {
            System.Diagnostics.Debug.WriteLine("SER INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x3C
        public static void MER()
        {
            System.Diagnostics.Debug.WriteLine("MER INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x3D
        public static void DER()
        {
            System.Diagnostics.Debug.WriteLine("DER INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x3E
        public static void AUR()
        {
            System.Diagnostics.Debug.WriteLine("AUR INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x3F
        public static void SUR()
        {
            System.Diagnostics.Debug.WriteLine("SUR INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x40
        public static void STH()
        {
            System.Diagnostics.Debug.WriteLine("STH INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x41
        public static void LA()
        {
            CPU.Registers[CPU.R1] = CPU.D1 + CPU.Registers[CPU.X1] + CPU.Registers[CPU.B1];

            CPU.Head += 4;
        }

        // 0x42
        public static void STC()
        {
            System.Diagnostics.Debug.WriteLine("STC INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x43
        public static void IC()
        {
            System.Diagnostics.Debug.WriteLine("IC INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x44
        public static void EX()
        {
            System.Diagnostics.Debug.WriteLine("EX INSTRUCTION NOT IMPLEMENTED");
        }

        public static void BAL()
        {
            CPU.Registers[CPU.R1] = CPU.Head + 4;

            CPU.Head = CPU.DXB1;
        }

        // 0x46
        public static void BCT()
        {
            System.Diagnostics.Debug.WriteLine("BCT INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x47
        public static void BC()
        {
            if ((CPU.ConditionCode & CPU.R1) > 0)
            {
                CPU.Head = CPU.DXB1;
            }
            else
            {
                CPU.Head += 4;
            }
        }

        // 0x48
        public static void LH()
        {
            System.Diagnostics.Debug.WriteLine("LH INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x49
        public static void CH()
        {
            System.Diagnostics.Debug.WriteLine("CH INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x4A
        public static void AH()
        {
            System.Diagnostics.Debug.WriteLine("AH INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x4B
        public static void SH()
        {
            System.Diagnostics.Debug.WriteLine("SH INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x4C
        public static void MH()
        {
            System.Diagnostics.Debug.WriteLine("MH INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x4D
        public static void BAS()
        {
            System.Diagnostics.Debug.WriteLine("BAS INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x4E
        public static void CVD()
        {
            System.Diagnostics.Debug.WriteLine("CVD INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x4F
        public static void CVB()
        {
            System.Diagnostics.Debug.WriteLine("CVB INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x50
        public static void ST()
        {
            CPU.WriteMemory(CPU.DXB1, BitConverter.GetBytes(CPU.Registers[CPU.R1]).Reverse().ToArray());

            CPU.Head += 4;
        }

        // 0x54
        public static void N()
        {
            System.Diagnostics.Debug.WriteLine("N INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x55
        public static void CL()
        {
            System.Diagnostics.Debug.WriteLine("CL INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x56
        public static void O()
        {
            System.Diagnostics.Debug.WriteLine("O INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x57
        public static void X()
        {
            System.Diagnostics.Debug.WriteLine("X INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x58
        public static void L()
        {
            if (CPU.Bytecode.Count < CPU.DXB1) throw new Exception(CPU.DXB1 + " was OUT OF BOUNDS");
            CPU.Registers[CPU.R1] = BitConverter.ToInt32(CPU.ReadMemory(CPU.DXB1, 4));

            CPU.Head += 4;
        }

        // 0x59
        public static void C()
        {
            int compare = BitConverter.ToInt32(CPU.ReadMemory(CPU.DXB1, 4));
            if (CPU.Registers[CPU.R1] == compare)
            {
                CPU.SetCC(1, 0, 0, 0);
            }
            else if (CPU.Registers[CPU.R1] < compare)
            {
                CPU.SetCC(0, 1, 0, 0);
            }
            else
            {
                CPU.SetCC(0, 0, 1, 0);
            }

            CPU.Head += 4;
        }

        // 0x5A
        public static void A()
        {
            if (CPU.Bytecode.Count < CPU.DXB1) throw new Exception(CPU.DXB1 + " was OUT OF BOUNDS");
            CPU.Registers[CPU.R1] += BitConverter.ToInt32(CPU.ReadMemory(CPU.DXB1, 4));

            CPU.Head += 4;
        }

        // 0x5B
        public static void S()
        {
            if (CPU.Bytecode.Count < CPU.DXB1) throw new Exception(CPU.DXB1 + " was OUT OF BOUNDS");
            CPU.Registers[CPU.R1] -= BitConverter.ToInt32(CPU.ReadMemory(CPU.DXB1, 4));

            CPU.Head += 4;
        }

        // 0x5C
        public static void M()
        {
            if (CPU.R1 % 2 != 0) throw new Exception("Cannot multiply using an odd register");

            int multiplicand = CPU.Registers[CPU.R1 + 1];
            int multiplier = BitConverter.ToInt32(CPU.ReadMemory(CPU.DXB1, 4));

            long result = multiplicand * multiplier;

            CPU.Registers[CPU.R1] = (int)(result >> 32);
            CPU.Registers[CPU.R1+1] = (int)result;

            CPU.Head += 4;
        }

        // 0x5D
        public static void D()
        {
            if (CPU.R1 % 2 != 0) throw new Exception("Cannot divide using an odd register");

            long dividend = (long)CPU.Registers[CPU.R1] << 32 | (long)(uint)CPU.Registers[CPU.R1 + 1];
            int divisor = BitConverter.ToInt32(CPU.ReadMemory(CPU.DXB1, 4));

            if (divisor == 0) throw new Exception("Cannot divide by zero");

            // System.Diagnostics.Debug.WriteLine(dividend + " / " + divisor + ": " + (dividend / divisor) + " (" + (dividend % divisor) + ")");

            CPU.Registers[CPU.R1] = (int)(dividend % divisor);
            CPU.Registers[CPU.R1 + 1] = (int)(dividend / divisor);

            CPU.Head += 4;
        }

        // 0x5E
        public static void AL()
        {
            System.Diagnostics.Debug.WriteLine("AL INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x5F
        public static void SL()
        {
            System.Diagnostics.Debug.WriteLine("SL INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x60
        public static void STD()
        {
            System.Diagnostics.Debug.WriteLine("STD INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x67
        public static void MXD()
        {
            System.Diagnostics.Debug.WriteLine("MXD INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x68
        public static void LD()
        {
            System.Diagnostics.Debug.WriteLine("LD INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x69
        public static void CD()
        {
            System.Diagnostics.Debug.WriteLine("CD INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x6A
        public static void AD()
        {
            System.Diagnostics.Debug.WriteLine("AD INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x6B
        public static void SD()
        {
            System.Diagnostics.Debug.WriteLine("SD INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x6C
        public static void MD()
        {
            System.Diagnostics.Debug.WriteLine("MD INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x6D
        public static void DD()
        {
            System.Diagnostics.Debug.WriteLine("DD INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x6E
        public static void AW()
        {
            System.Diagnostics.Debug.WriteLine("AW INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x6F
        public static void SW()
        {
            System.Diagnostics.Debug.WriteLine("SW INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x70
        public static void STE()
        {
            System.Diagnostics.Debug.WriteLine("STE INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x78
        public static void LE()
        {
            System.Diagnostics.Debug.WriteLine("LE INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x79
        public static void CE()
        {
            System.Diagnostics.Debug.WriteLine("CE INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x7A
        public static void AE()
        {
            System.Diagnostics.Debug.WriteLine("AE INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x7B
        public static void SE()
        {
            System.Diagnostics.Debug.WriteLine("SE INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x7C
        public static void ME()
        {
            System.Diagnostics.Debug.WriteLine("ME INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x7D
        public static void DE()
        {
            System.Diagnostics.Debug.WriteLine("DE INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x7E
        public static void AU()
        {
            System.Diagnostics.Debug.WriteLine("AU INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x7F
        public static void SU()
        {
            System.Diagnostics.Debug.WriteLine("SU INSTRUCTION NOT IMPLEMENTED");
        }

        //Add("SSM", 0x80, SSM, OperationType.S);
        //Add("LPSW", 0x82, LPSW, OperationType.S);
        //Add("DIAGNOSE", 0x83, DIAGNOSE, OperationType.RX);
        // 0x84
        public static void WRD()
        {
            System.Diagnostics.Debug.WriteLine("WRD INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x85
        public static void RDD()
        {
            System.Diagnostics.Debug.WriteLine("RDD INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x86
        public static void BXH()
        {
            System.Diagnostics.Debug.WriteLine("BXH INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x87
        public static void BXLE()
        {
            System.Diagnostics.Debug.WriteLine("BXLE INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x88
        public static void SRL()
        {
            System.Diagnostics.Debug.WriteLine("SRL INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x89
        public static void SLL()
        {
            System.Diagnostics.Debug.WriteLine("SLL INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x8A
        public static void SRA()
        {
            System.Diagnostics.Debug.WriteLine("SRA INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x8B
        public static void SLA()
        {
            System.Diagnostics.Debug.WriteLine("SLA INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x8C
        public static void SRDL()
        {
            System.Diagnostics.Debug.WriteLine("SRDL INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x8D
        public static void SLDL()
        {
            System.Diagnostics.Debug.WriteLine("SLDL INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x8E
        public static void SRDA()
        {
            System.Diagnostics.Debug.WriteLine("SRDA INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x8F
        public static void SLDA()
        {
            System.Diagnostics.Debug.WriteLine("SLDA INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x90
        public static void STM()
        {
            System.Diagnostics.Debug.WriteLine("STM INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x91
        public static void TM()
        {
            System.Diagnostics.Debug.WriteLine("TM INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x92
        public static void MVI()
        {
            CPU.WriteMemory(CPU.DXB1, new byte[1] { (byte)CPU.I });

            CPU.Head += 4;
        }

        // 0x93
        public static void TS()
        {
            System.Diagnostics.Debug.WriteLine("TS INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x94
        public static void NI()
        {
            System.Diagnostics.Debug.WriteLine("NI INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x95
        public static void CLI()
        {
            System.Diagnostics.Debug.WriteLine("CLI INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x96
        public static void OI()
        {
            System.Diagnostics.Debug.WriteLine("OI INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x97
        public static void XI()
        {
            System.Diagnostics.Debug.WriteLine("XI INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x98
        public static void LM()
        {
            // get total number of registers including the loop around
            int total = CPU.R2 - CPU.R1;
            if (total < 0) total = (16 - CPU.R1) + CPU.R2;

            int B1val = CPU.Registers[CPU.B1];

            for (int i = 0; i <= total; ++i) // loop through registers (loop around 15 back to zero)
            {
                int reg = (CPU.R1 + i) & 0xF;
                CPU.Registers[reg] = BitConverter.ToInt32(CPU.ReadMemory(CPU.D1 + B1val + (i * 4), 4));
            }

            CPU.Head += 4;
        }

        // 0x9C
        public static void SIO()
        {
            System.Diagnostics.Debug.WriteLine("SIO INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x9D
        public static void TIO()
        {
            System.Diagnostics.Debug.WriteLine("TIO INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x9E
        public static void HIO()
        {
            System.Diagnostics.Debug.WriteLine("HIO INSTRUCTION NOT IMPLEMENTED");
        }

        // 0x9F
        public static void TCH()
        {
            System.Diagnostics.Debug.WriteLine("TCH INSTRUCTION NOT IMPLEMENTED");
        }

        // 0xD1
        public static void MVN()
        {
            System.Diagnostics.Debug.WriteLine("MVN INSTRUCTION NOT IMPLEMENTED");
        }

        // 0xD2
        public static void MVC()
        {
            for (int i = 0; i <= CPU.L; ++i) CPU.Bytecode[CPU.DB1 + i] = CPU.Bytecode[CPU.DB2 + i];

            CPU.Head += 6;
        }

        // 0xD3
        public static void MVZ()
        {
            System.Diagnostics.Debug.WriteLine("MVZ INSTRUCTION NOT IMPLEMENTED");
        }

        // 0xD4
        public static void NC()
        {
            System.Diagnostics.Debug.WriteLine("NC INSTRUCTION NOT IMPLEMENTED");
        }

        // 0xD5
        public static void CLC()
        {
            System.Diagnostics.Debug.WriteLine("CLC INSTRUCTION NOT IMPLEMENTED");
        }

        // 0xD6
        public static void OC()
        {
            System.Diagnostics.Debug.WriteLine("OC INSTRUCTION NOT IMPLEMENTED");
        }

        // 0xD7
        public static void XC()
        {
            System.Diagnostics.Debug.WriteLine("XC INSTRUCTION NOT IMPLEMENTED");
        }

        // 0xDC
        public static void TR()
        {
            System.Diagnostics.Debug.WriteLine("TR INSTRUCTION NOT IMPLEMENTED");
        }

        // 0xDD
        public static void TRT()
        {
            System.Diagnostics.Debug.WriteLine("TRT INSTRUCTION NOT IMPLEMENTED");
        }

        // 0xDE
        public static void ED()
        {
            System.Diagnostics.Debug.WriteLine("ED INSTRUCTION NOT IMPLEMENTED");
        }

        // 0xDF
        public static void EDMK()
        {
            System.Diagnostics.Debug.WriteLine("EDMK INSTRUCTION NOT IMPLEMENTED");
        }

        // 0xE2
        public static void UNPKU()
        {
            System.Diagnostics.Debug.WriteLine("UNPKU INSTRUCTION NOT IMPLEMENTED");
        }

        // 0xF0
        public static void SRP()
        {
            System.Diagnostics.Debug.WriteLine("SRP INSTRUCTION NOT IMPLEMENTED");
        }

        // 0xF1
        public static void MVO()
        {
            System.Diagnostics.Debug.WriteLine("MVO INSTRUCTION NOT IMPLEMENTED");
        }

        // 0xF2
        public static void PACK()
        {
            System.Diagnostics.Debug.WriteLine("PACK INSTRUCTION NOT IMPLEMENTED");
        }

        // 0xF3
        public static void UNPK()
        {
            System.Diagnostics.Debug.WriteLine("UNPK INSTRUCTION NOT IMPLEMENTED");
        }

        // 0xF8
        public static void ZAP()
        {
            System.Diagnostics.Debug.WriteLine("ZAP INSTRUCTION NOT IMPLEMENTED");
        }

        // 0xF9
        public static void CP()
        {
            System.Diagnostics.Debug.WriteLine("CP INSTRUCTION NOT IMPLEMENTED");
        }

        // 0xFA
        public static void AP()
        {
            System.Diagnostics.Debug.WriteLine("AP INSTRUCTION NOT IMPLEMENTED");
        }

        // 0xFB
        public static void SP()
        {
            System.Diagnostics.Debug.WriteLine("SP INSTRUCTION NOT IMPLEMENTED");
        }

        // 0xFC
        public static void MP()
        {
            System.Diagnostics.Debug.WriteLine("MP INSTRUCTION NOT IMPLEMENTED");
        }

        // 0xFD
        public static void DP()
        {
            System.Diagnostics.Debug.WriteLine("DP INSTRUCTION NOT IMPLEMENTED");
        }

        private static void Add(string mnemonic, byte bytecode, Action action, OperationType type)
        {
            Mnemonics.Add(mnemonic, bytecode);
            Operations[bytecode] = action;
            OperationTypes[bytecode] = type;
        }
        static Operation()
        {
            // CUSTOM OPERATIONS
            Add("XDECO", 0x52, XDECO, OperationType.RX);
            Add("XDECI", 0x53, XDECI, OperationType.RX);
            Add("XREAD", 0xE0, XIO, OperationType.XX);
            Add("XPRNT", 0xE0, XIO, OperationType.XX);
            Add("XDUMP", 0xE1, XDUMP, OperationType.XX);

            // STANDARD OPERATIONS
            Add("SPM", 0x04, SPM, OperationType.RR);
            Add("BALR", 0x05, BALR, OperationType.RR);
            Add("BCTR", 0x06, BCTR, OperationType.RR);
            Add("BCR", 0x07, BCR, OperationType.RR);
            Add("NOPR", 0x07, BCR, OperationType.RR);
            Add("BR", 0x07, BCR, OperationType.RR);
            Add("SSK", 0x08, SSK, OperationType.RR);
            Add("ISK", 0x09, ISK, OperationType.RR);
            Add("SVC", 0x0A, SVC, OperationType.I);
            Add("BASR", 0x0D, BASR, OperationType.RR);
            Add("LPR", 0x10, LPR, OperationType.RR);
            Add("LNR", 0x11, LNR, OperationType.RR);
            Add("LTR", 0x12, LTR, OperationType.RR);
            Add("LCR", 0x13, LCR, OperationType.RR);
            Add("NR", 0x14, NR, OperationType.RR);
            Add("CLR", 0x15, CLR, OperationType.RR);
            Add("OR", 0x16, OR, OperationType.RR);
            Add("XR", 0x17, XR, OperationType.RR);
            Add("LR", 0x18, LR, OperationType.RR);
            Add("CR", 0x19, CR, OperationType.RR);
            Add("AR", 0x1A, AR, OperationType.RR);
            Add("SR", 0x1B, SR, OperationType.RR);
            Add("MR", 0x1C, MR, OperationType.RR);
            Add("DR", 0x1D, DR, OperationType.RR);
            Add("ALR", 0x1E, ALR, OperationType.RR);
            Add("SLR", 0x1F, SLR, OperationType.RR);
            Add("LPDR", 0x20, LPDR, OperationType.RR);
            Add("LNDR", 0x21, LNDR, OperationType.RR);
            Add("LTDR", 0x22, LTDR, OperationType.RR);
            Add("LCDR", 0x23, LCDR, OperationType.RR);
            Add("HDR", 0x24, HDR, OperationType.RR);
            Add("LRDR", 0x25, LRDR, OperationType.RR);
            Add("MXR", 0x26, MXR, OperationType.RR);
            Add("MXDR", 0x27, MXDR, OperationType.RR);
            Add("LDR", 0x28, LDR, OperationType.RR);
            Add("CDR", 0x29, CDR, OperationType.RR);
            Add("ADR", 0x2A, ADR, OperationType.RR);
            Add("SDR", 0x2B, SDR, OperationType.RR);
            Add("MDR", 0x2C, MDR, OperationType.RR);
            Add("DDR", 0x2D, DDR, OperationType.RR);
            Add("AWR", 0x2E, AWR, OperationType.RR);
            Add("SWR", 0x2F, SWR, OperationType.RR);
            Add("LPER", 0x30, LPER, OperationType.RR);
            Add("LNER", 0x31, LNER, OperationType.RR);
            Add("LTER", 0x32, LTER, OperationType.RR);
            Add("LCER", 0x33, LCER, OperationType.RR);
            Add("HER", 0x34, HER, OperationType.RR);
            Add("LRER", 0x35, LRER, OperationType.RR);
            Add("AXR", 0x36, AXR, OperationType.RR);
            Add("SXR", 0x37, SXR, OperationType.RR);
            Add("LER", 0x38, LER, OperationType.RR);
            Add("CER", 0x39, CER, OperationType.RR);
            Add("AER", 0x3A, AER, OperationType.RR);
            Add("SER", 0x3B, SER, OperationType.RR);
            Add("MER", 0x3C, MER, OperationType.RR);
            Add("DER", 0x3D, DER, OperationType.RR);
            Add("AUR", 0x3E, AUR, OperationType.RR);
            Add("SUR", 0x3F, SUR, OperationType.RR);
            Add("STH", 0x40, STH, OperationType.RX);
            Add("LA", 0x41, LA, OperationType.RX);
            Add("STC", 0x42, STC, OperationType.RX);
            Add("IC", 0x43, IC, OperationType.RX);
            Add("EX", 0x44, EX, OperationType.RX);
            Add("BAL", 0x45, BAL, OperationType.RX);
            Add("BCT", 0x46, BCT, OperationType.RX);
            Add("BC", 0x47, BC, OperationType.RX);
            Add("NOP", 0x47, BC, OperationType.RX);
            Add("BO", 0x47, BC, OperationType.RX);
            Add("BH", 0x47, BC, OperationType.RX);
            Add("BP", 0x47, BC, OperationType.RX);
            Add("BL", 0x47, BC, OperationType.RX);
            Add("BM", 0x47, BC, OperationType.RX);
            Add("BNE", 0x47, BC, OperationType.RX);
            Add("BNZ", 0x47, BC, OperationType.RX);
            Add("BE", 0x47, BC, OperationType.RX);
            Add("BZ", 0x47, BC, OperationType.RX);
            Add("BNL", 0x47, BC, OperationType.RX);
            Add("BNM", 0x47, BC, OperationType.RX);
            Add("BNH", 0x47, BC, OperationType.RX);
            Add("BNP", 0x47, BC, OperationType.RX);
            Add("BNO", 0x47, BC, OperationType.RX);
            Add("B", 0x47, BC, OperationType.RX);
            Add("LH", 0x48, LH, OperationType.RX);
            Add("CH", 0x49, CH, OperationType.RX);
            Add("AH", 0x4A, AH, OperationType.RX);
            Add("SH", 0x4B, SH, OperationType.RX);
            Add("MH", 0x4C, MH, OperationType.RX);
            Add("BAS", 0x4D, BAS, OperationType.RX);
            Add("CVD", 0x4E, CVD, OperationType.RX);
            Add("CVB", 0x4F, CVB, OperationType.RX);
            Add("ST", 0x50, ST, OperationType.RX);
            Add("N", 0x54, N, OperationType.RX);
            Add("CL", 0x55, CL, OperationType.RX);
            Add("O", 0x56, O, OperationType.RX);
            Add("X", 0x57, X, OperationType.RX);
            Add("L", 0x58, L, OperationType.RX);
            Add("C", 0x59, C, OperationType.RX);
            Add("A", 0x5A, A, OperationType.RX);
            Add("S", 0x5B, S, OperationType.RX);
            Add("M", 0x5C, M, OperationType.RX);
            Add("D", 0x5D, D, OperationType.RX);
            Add("AL", 0x5E, AL, OperationType.RX);
            Add("SL", 0x5F, SL, OperationType.RX);
            Add("STD", 0x60, STD, OperationType.RX);
            Add("MXD", 0x67, MXD, OperationType.RX);
            Add("LD", 0x68, LD, OperationType.RX);
            Add("CD", 0x69, CD, OperationType.RX);
            Add("AD", 0x6A, AD, OperationType.RX);
            Add("SD", 0x6B, SD, OperationType.RX);
            Add("MD", 0x6C, MD, OperationType.RX);
            Add("DD", 0x6D, DD, OperationType.RX);
            Add("AW", 0x6E, AW, OperationType.RX);
            Add("SW", 0x6F, SW, OperationType.RX);
            Add("STE", 0x70, STE, OperationType.RX);
            Add("LE", 0x78, LE, OperationType.RX);
            Add("CE", 0x79, CE, OperationType.RX);
            Add("AE", 0x7A, AE, OperationType.RX);
            Add("SE", 0x7B, SE, OperationType.RX);
            Add("ME", 0x7C, ME, OperationType.RX);
            Add("DE", 0x7D, DE, OperationType.RX);
            Add("AU", 0x7E, AU, OperationType.RX);
            Add("SU", 0x7F, SU, OperationType.RX);
            //Add("SSM", 0x80, SSM, OperationType.S);
            //Add("LPSW", 0x82, LPSW, OperationType.S);
            //Add("DIAGNOSE", 0x83, DIAGNOSE, OperationType.RX);
            Add("WRD", 0x84, WRD, OperationType.RX);
            Add("RDD", 0x85, RDD, OperationType.RX);
            Add("BXH", 0x86, BXH, OperationType.RS);
            Add("BXLE", 0x87, BXLE, OperationType.RS);
            Add("SRL", 0x88, SRL, OperationType.RS);
            Add("SLL", 0x89, SLL, OperationType.RS);
            Add("SRA", 0x8A, SRA, OperationType.RS);
            Add("SLA", 0x8B, SLA, OperationType.RS);
            Add("SRDL", 0x8C, SRDL, OperationType.RS);
            Add("SLDL", 0x8D, SLDL, OperationType.RS);
            Add("SRDA", 0x8E, SRDA, OperationType.RS);
            Add("SLDA", 0x8F, SLDA, OperationType.RS);
            Add("STM", 0x90, STM, OperationType.RS);
            Add("TM", 0x91, TM, OperationType.SI);
            Add("MVI", 0x92, MVI, OperationType.SI);
            Add("TS", 0x93, TS, OperationType.SI);
            Add("NI", 0x94, NI, OperationType.SI);
            Add("CLI", 0x95, CLI, OperationType.SI);
            Add("OI", 0x96, OI, OperationType.SI);
            Add("XI", 0x97, XI, OperationType.RSI);
            Add("LM", 0x98, LM, OperationType.RS);
            Add("SIO", 0x9C, SIO, OperationType.SI);
            Add("TIO", 0x9D, TIO, OperationType.SI);
            Add("HIO", 0x9E, HIO, OperationType.SI);
            Add("TCH", 0x9F, TCH, OperationType.SI);
            Add("MVN", 0xD1, MVN, OperationType.SS);
            Add("MVC", 0xD2, MVC, OperationType.SS);
            Add("MVZ", 0xD3, MVZ, OperationType.SS);
            Add("NC", 0xD4, NC, OperationType.SS);
            Add("CLC", 0xD5, CLC, OperationType.SS);
            Add("OC", 0xD6, OC, OperationType.SS);
            Add("XC", 0xD7, XC, OperationType.SS);
            Add("TR", 0xDC, TR, OperationType.SS);
            Add("TRT", 0xDD, TRT, OperationType.SS);
            Add("ED", 0xDE, ED, OperationType.SS);
            Add("EDMK", 0xDF, EDMK, OperationType.SS);
            Add("UNPKU", 0xE2, UNPKU, OperationType.SS);
            Add("SRP", 0xF0, SRP, OperationType.SS);
            Add("MVO", 0xF1, MVO, OperationType.SS);
            Add("PACK", 0xF2, PACK, OperationType.SS);
            Add("UNPK", 0xF3, UNPK, OperationType.SS);
            Add("ZAP", 0xF8, ZAP, OperationType.SS);
            Add("CP", 0xF9, CP, OperationType.SS);
            Add("AP", 0xFA, AP, OperationType.SS);
            Add("SP", 0xFB, SP, OperationType.SS);
            Add("MP", 0xFC, MP, OperationType.SS);
            Add("DP", 0xFD, DP, OperationType.SS);
        }
    }
}
