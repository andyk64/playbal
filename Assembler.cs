﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace PlayBAL
{
    class Assembler
    {
        private List<byte> bytecode = new();
        private List<string> assembly = new();

        // labels and their references
        private Dictionary<string, int> labels; // LABEL to byte pos
        private List<Tuple<int, string, Operation.OperationType>> labelRefs; // reference line number to LABEL (bool IsDXB)

        private Dictionary<string, int> literals = new(); // literal = to byte pos
        private List<Tuple<int, string>> literalRefs = new(); // line number to literal =


        private List<string> asmPass1 = new();

        public List<string> assemblyLines => assembly;

        public List<string> bytecodeLines = new();

        private int bytePos = 0;
        private Dictionary<int, int> bytePosToLineNum;

        public Assembler()
        {
            labels = new();
            labelRefs = new();
        }

        public List<byte> Assemble(string[] assembly_code)
        {
            // // // //
            // PASS 1
            bytePos = 0;
            for (int i = 0; i < assembly_code.Length; ++i)
            {
                var line = assembly_code[i].TrimEnd();
                if (line.StartsWith("*") || line.StartsWith("/") || line.Length < 10)
                {
                    asmPass1.Add(line);
                    continue;
                }

                // LABEL
                var labelMatch = Regex.Match(line.Substring(0, 9), @"^[A-Z][A-Z0-9]{0,7}\s+$|^\s{9}");
                if (!labelMatch.Success) throw new AssemblerException(i, "Label format incorrect");
                var label = labelMatch.Value.Trim();
                if (label != string.Empty) labels[label] = bytePos;

                // OPERATION
                var operationMatch = Regex.Match(line.Substring(9, Math.Min(6, line.Length - 9)), @"^[A-Z]{1,5}\s*$");
                if (!operationMatch.Success) throw new AssemblerException(i, "Instruction mnemonic format incorrect");
                var operation = operationMatch.Value.Trim();

                var bytes = string.Empty;
                if (Operation.Mnemonics.ContainsKey(operation))
                {
                    var opcode = Operation.Mnemonics[operation];

                    // Add short-hand masks to input string
                    if (opcode == 0x07)
                    {
                        switch (operation)
                        {
                            case "NOPR":
                                line = line.Remove(9, 6);
                                line = line.Insert(9, "BCR   0,");
                                break;
                            case "BR":
                                line = line.Remove(9, 6);
                                line = line.Insert(9, "BCR   15,");
                                break;
                        }
                    }
                    else if (opcode == 0x47)
                    {
                        switch (operation)
                        {
                            case "NOP":
                                line = line.Remove(9, 6);
                                line = line.Insert(9, "BC    0,");
                                break;
                            case "BO":
                                line = line.Remove(9, 6);
                                line = line.Insert(9, "BC    1,");
                                break;
                            case "BH":
                                line = line.Remove(9, 6);
                                line = line.Insert(9, "BC    2,");
                                break;
                            case "BP":
                                line = line.Remove(9, 6);
                                line = line.Insert(9, "BC    2,");
                                break;
                            case "BL":
                                line = line.Remove(9, 6);
                                line = line.Insert(9, "BC    4,");
                                break;
                            case "BM":
                                line = line.Remove(9, 6);
                                line = line.Insert(9, "BC    4,");
                                break;
                            case "BNE":
                                line = line.Remove(9, 6);
                                line = line.Insert(9, "BC    7,");
                                break;
                            case "BNZ":
                                line = line.Remove(9, 6);
                                line = line.Insert(9, "BC    7,");
                                break;
                            case "BE":
                                line = line.Remove(9, 6);
                                line = line.Insert(9, "BC    8,");
                                break;
                            case "BX":
                                line = line.Remove(9, 6);
                                line = line.Insert(9, "BC    8,");
                                break;
                            case "BNL":
                                line = line.Remove(9, 6);
                                line = line.Insert(9, "BC    11,");
                                break;
                            case "BNM":
                                line = line.Remove(9, 6);
                                line = line.Insert(9, "BC    11,");
                                break;
                            case "BNH":
                                line = line.Remove(9, 6);
                                line = line.Insert(9, "BC    13,");
                                break;
                            case "BNP":
                                line = line.Remove(9, 6);
                                line = line.Insert(9, "BC    13,");
                                break;
                            case "BNO":
                                line = line.Remove(9, 6);
                                line = line.Insert(9, "BC    14,");
                                break;
                            case "B":
                                line = line.Remove(9, 6);
                                line = line.Insert(9, "BC    15,");
                                break;
                        }
                    }
                    else if (opcode == 0xE0)
                    {
                        switch (operation)
                        {
                            case "XREAD":
                                line = line.Remove(9, 6);
                                line = line.Insert(9, "XREAD 0,");
                                break;
                            case "XPRNT":
                                line = line.Remove(9, 6);
                                line = line.Insert(9, "XPRNT 2,");
                                break;
                        }
                    }

                    var parameters = SplitParameters(line);
                    foreach (var parameter in parameters)
                    {
                        if (parameter.StartsWith('='))
                        {
                            // THIS IS A LITERAL PARAMETER
                            literalRefs.Add(new Tuple<int, string>(asmPass1.Count, parameter));
                        }
                        else if (char.IsLetter(parameter[0]))
                        {
                            if (parameter.Contains("'"))
                            {
                                // THIS IS AN IMMEDIATE
                                line = line.Replace(parameter, ParseImmediate(parameter).ToString());
                            }
                            else
                            {
                                // THIS IS A LABEL PARAMETER
                                var optype = Operation.GetType(opcode);
                                labelRefs.Add(new Tuple<int, string, Operation.OperationType>(asmPass1.Count, parameter, optype));
                            }
                        }
                    }

                    var length = Operation.GetLength(opcode);
                    bytePos += length;
                    asmPass1.Add(line);
                }
                else if (operation == "DS" || operation == "DC")
                {
                    var parameterMatch = Regex.Match(line[15..], @"^=?([0-9]*)([A-Z])(L(\d+))?('([^']+)')?");
                    if (!parameterMatch.Success) throw new AssemblerException(i, "Parameter format incorrect");

                    var parts = ParseLiteral(parameterMatch.Value);
                    var length = int.Parse(parts[0]) * int.Parse(parts[2]);
                    
                    if (parts[1] == "F" && bytePos % 4 != 0)
                    {
                        bytePos += 4 - (bytePos % 4);
                        if (labels.ContainsKey(label)) labels[label] += 4 - (bytePos % 4);
                    }
                    bytePos += length;
                    asmPass1.Add(line);
                }
                else if (operation == "LTORG")
                {
                    foreach (var literal in literalRefs)
                    {
                        if (literals.ContainsKey(literal.Item2)) continue; // only make unique literals

                        // calculate bytes here and then add

                        var parts = ParseLiteral(literal.Item2);
                        var length = int.Parse(parts[0]) * int.Parse(parts[2]);

                        if (parts[1] == "F" && bytePos % 4 != 0)
                        {
                            bytePos += 4 - (bytePos % 4);
                        }

                        literals[literal.Item2] = bytePos;
                        bytePos += length;
                        asmPass1.Add("         DC    " + literal.Item2[1..]);
                    }
                }
                else
                {
                    asmPass1.Add(line);
                    //AddLine(line, bytes);
                }
            }

            // // // //
            // PASS 2
            foreach (var reference in labelRefs)
            {
                var label = string.Concat(reference.Item2.TakeWhile(char.IsLetterOrDigit));
                var extra = reference.Item2.Split("(")[0][label.Length..];
                var length = "0";
                if (reference.Item2.Contains("(")) length = int.Parse(reference.Item2.Split("(")?[1].Split(")")[0]).ToString();

                if (labels.ContainsKey(label))
                {
                    var displacement = labels[label];
                    if (extra.Length > 0) displacement += int.Parse(extra);

                    if (reference.Item3 == Operation.OperationType.SS)
                    {
                        if (reference.Item2.Contains("("))
                        {
                            asmPass1[reference.Item1] = asmPass1[reference.Item1].Replace(reference.Item2, displacement + "(" + (int.Parse(length)-1) + ",15)");
                        }
                        else
                        {
                            asmPass1[reference.Item1] = asmPass1[reference.Item1].Replace(reference.Item2, displacement + "(15)");
                        }
                    }
                    else if (reference.Item3 == Operation.OperationType.RS || reference.Item3 == Operation.OperationType.SI)
                    {
                        asmPass1[reference.Item1] = asmPass1[reference.Item1].Replace(reference.Item2, displacement + "(15)");
                    }
                    else
                    {
                        asmPass1[reference.Item1] = asmPass1[reference.Item1].Replace(reference.Item2, displacement + "(0,15)");
                    }
                }
            }

            if (literals.Count < 1) throw new AssemblerException(0, "No LTORG statement in assembly");
            foreach (var literalRef in literalRefs)
            {
                if (literals.ContainsKey(literalRef.Item2))
                {
                    string replaceStr = literals[literalRef.Item2].ToString() + "(0,15)";

                    asmPass1[literalRef.Item1] = asmPass1[literalRef.Item1].Replace(literalRef.Item2, replaceStr);
                }
            }

            // // // //
            // PASS 3
            bytePos = 0;
            assemblyLines.Clear();
            bytecodeLines.Clear();
            for (int i = 0; i < asmPass1.Count; ++i)
            {
                var line = asmPass1[i];
                if (line.StartsWith("*") || line.StartsWith("/") || line.Length < 16)
                {
                    AddLine(line, string.Empty);
                    continue;
                }

                var operation = Regex.Match(line.Substring(9, Math.Min(6, line.Length - 9)), @"^[A-Z]{1,5}\s*$").Value.Trim();
                var parameters = SplitParameters(line);
                var bytes = string.Empty;

                if (Operation.Mnemonics.ContainsKey(operation))
                {
                    var opcode = Operation.Mnemonics[operation];
                    bytes += opcode.ToString("X2");

                    switch (Operation.GetType(opcode))
                    {
                        case Operation.OperationType.RR:
                            bytes += Encode(parameters[0], parameters[1]);
                            break;
                        case Operation.OperationType.RX:
                            bytes += EncodeNibble(parameters[0]);
                            bytes += EncodeDXB(parameters[1]);
                            break;
                        case Operation.OperationType.RS:
                            bytes += EncodeNibble(parameters[0]);
                            bytes += EncodeNibble(parameters[1]);
                            bytes += EncodeDB(parameters[2]);
                            break;
                        case Operation.OperationType.XX:
                            bytes += EncodeNibble(parameters[0]);
                            bytes += EncodeDXB(parameters[1]);
                            bytes += EncodeHalf(parameters[2]);
                            break;
                        case Operation.OperationType.SI:
                            bytes += EncodeByte(parameters[1]);
                            bytes += EncodeDB(parameters[0]);
                            break;
                        case Operation.OperationType.SS:
                            bytes += EncodeDXL(parameters[0]);
                            bytes += EncodeDB(parameters[1]);
                            break;
                        default:
                            break;
                    }

                    bytePos += bytes.Length / 2;
                    AddLine(line, bytes);
                }
                else if (operation == "DS" || operation == "DC")
                {
                    var parameterMatch = Regex.Match(line[15..], @"^=?([0-9]*)([A-Z])(L(\d+))?('([^']+)')?");
                    if (!parameterMatch.Success) throw new AssemblerException(i, "Parameter format incorrect");

                    var parts = ParseLiteral(parameterMatch.Value);
                    var length = int.Parse(parts[0]) * int.Parse(parts[2]);
                    
                    if (parts[1] == "F" && bytePos % 4 != 0)
                    {
                        bytePos += 4 - (bytePos % 4);
                        AddLine("               (FULLWORD ALIGNMENT)", new string('0', (4 - (bytePos % 4))));
                    }

                    if (parts[1] == "F" && parts[3].Length > 0)
                    {
                        for (int count = 0; count < length / 4; ++count) bytes += int.Parse(parts[3]).ToString("X8");
                    }
                    else if (parts[1] == "C" && parts[3].Length > 0)
                    {
                        var str = CPU.EBCDIC.GetBytes(parts[3]).ToList();
                        for (int cnt = 0; cnt < length - parts[3].Length; ++cnt) str.Add(0x40);
                        
                        foreach (var ch in str) bytes += ch.ToString("X2");
                    }
                    else
                    {
                        bytes += new string('0', length * 2);
                    }


                    bytePos += length;
                    AddLine(line, bytes);
                }
                else
                {
                    AddLine(line, string.Empty);
                }
            }

            return bytecode;
        }

        private static string EncodeNibble(string val)
        {
            int parsed = int.Parse(val);
            //if (val > 15) throw new AssemblerException(0, "Value is toob");
            return ((byte)parsed).ToString("X1");
        }

        private static string EncodeByte(string val)
        {
            int parsed = int.Parse(val);
            //if (val > 15) throw new AssemblerException(0, "Value is toob");
            return ((byte)parsed).ToString("X2");
        }

        private static string EncodeHalf(string val)
        {
            int parsed = int.Parse(val);
            //if (val > 15) throw new AssemblerException(0, "Value is toob");
            return ((byte)parsed).ToString("X4");
        }

        private static string EncodeDXB(string val)
        {
            var dxb = Regex.Match(val, @"^(\d+)\((\d+)(,(\d+))?\)$");

            string result = string.Empty;
            result += EncodeNibble(dxb.Groups[2].Value);
            result += EncodeNibble(dxb.Groups[4].Value);
            result += int.Parse(dxb.Groups[1].Value).ToString("X3");
            return result;
        }

        private static string EncodeDXL(string val)
        {
            var dxb = Regex.Match(val, @"^(\d+)\((\d+)(,(\d+))?\)$");

            string result = string.Empty;
            result += EncodeByte(dxb.Groups[2].Value);
            result += EncodeNibble(dxb.Groups[4].Value);
            result += int.Parse(dxb.Groups[1].Value).ToString("X3");
            return result;
        }

        private static string EncodeDB(string val)
        {
            var dxb = Regex.Match(val, @"^(\d+)\((\d+)(,(\d+))?\)$");

            string result = string.Empty;
            result += EncodeNibble(dxb.Groups[2].Value);
            result += int.Parse(dxb.Groups[1].Value).ToString("X3");
            return result;
        }

        private static string Encode(string left, string right)
        {
            byte result = (byte)(((int.Parse(left) & 0xF)) << 4);
            result += (byte)(int.Parse(right) & 0xF);
            return result.ToString("X2");
        }

        private static string Encode(string item)
        {
            byte result = (byte)(int.Parse(item) & 0xFF);
            return result.ToString("X2");
        }

        private void AddLine(string line, string bytecode)
        {
            if (bytecode == string.Empty)
            {
                assemblyLines.Add(line);
                bytecodeLines.Add("");
                return;
            }

            // ADD TO BYTECODE
            this.bytecode.AddRange(HexUtil.ToBytes(bytecode));

            string bytes = string.Empty;
            for (int i = 0; i < bytecode.Length; ++i)
            {
                bytes += bytecode[i];

                if (i % 4 == 3) bytes += " ";
                if (i % 16 == 15 || i == bytecode.Length - 1)
                {
                    bytecodeLines.Add(bytes);
                    //bytecodeLines.Add((bytePos - (bytecode.Length / 2)) + (i/16*8) + ": " + bytes);
                    bytes = string.Empty;
                }
            }

            assemblyLines.Add(line);
            for (int i = 0; i < Math.Ceiling((double)bytecode.Length / 16) - 1; ++i) assemblyLines.Add("               (" + line.Substring(0, Math.Min(30, line.Length)) + " . . . )");
        }

        private List<string> SplitParameters(string line)
        {
            Regex regTrim = new(@"\S+\'[^\']*\'|\S*");
            Regex regSplit = new(@"[A-Z0-9\+\-\*\/\=]+(\(\d+(,\d+)?\))?('.*')?");
            var matches = regSplit.Matches(regTrim.Match(line[15..]).Value)
                .Select(x => x.Value)
                .ToList();

            return matches;
        }

        private List<string> ParseLiteral(string literal)
        {
            var parts = new List<string>();

            // validate and group literal
            var reg = new Regex(@"^=?([0-9]*)([A-Z])(L(\d+))?('(.+)')?$");
            var result = reg.Match(literal);
            if (!result.Success) return parts;

            // add parts
            if (result.Groups[1].Value == string.Empty)
                parts.Add("1");
            else
                parts.Add(result.Groups[1].Value);

            parts.Add(result.Groups[2].Value);
            
            if (result.Groups[4].Value == string.Empty)
                switch (result.Groups[2].Value)
                {
                    case "F":
                        parts.Add("4");
                        break;
                    case "H":
                        parts.Add("2");
                        break;
                    case "C":
                        parts.Add("1");
                        break;
                    default:
                        break;
                }
            else
                parts.Add(result.Groups[4].Value);

            parts.Add(result.Groups[6].Value);

            return parts;
        }

        private byte ParseImmediate(string I)
        {
            var reg = Regex.Match(I, @"^([A-Z])'(.+)'$");
            if (!reg.Success) return 0x00;

            switch (reg.Groups[1].Value)
            {
                case "C":
                    return Encoding.GetEncoding("IBM037").GetBytes(reg.Groups[2].Value)[0];
                default:
                    break;
            }

            return 0x00;
        }


        private string LiteralToBytes(string literal, bool storage = false)
        {
            string bytes = string.Empty;

            // validate and group literal
            var reg = new Regex(@"^=?([0-9]*)([A-Z])(L?(\d+))?('(.+)')?$");
            var result = reg.Match(literal);
            if (!result.Success) return string.Empty;

            int.TryParse(result.Groups[1].Value, out int reps);
            if (result.Groups[1].Value == string.Empty) reps = 1;

            int.TryParse(result.Groups[4].Value, out int length);
            if (result.Groups[4].Value == string.Empty) length = -1;

            if (result.Groups[2].Value.Length < 1) return string.Empty;
            char type = result.Groups[2].Value[0];

            switch (type)
            {
                case 'F':
                    int.TryParse(result.Groups[6].Value, out int val);
                    if (length == -1) length = 4;
                    return new StringBuilder().Insert(0, val.ToString("X" + length), reps).ToString();
                case 'C':
                    string chars = result.Groups[6].Value;
                    if (length == -1)
                    {
                        return string.Join("", chars.Select(c => ((int)c).ToString("X2")));
                    }
                    else
                    {
                        chars = chars.Substring(0, Math.Min(chars.Length, length) - 1) + new string(' ', length - chars.Length);
                        return string.Join("", chars.Select(c => ((int)c).ToString("X2")));
                    }
            }

            return bytes;
        }
    }

    public class AssemblerException : Exception
    {
        public int LineNumber { get; set; }
        public AssemblerException(int line_number, string message = null) : base(message)
        {
            LineNumber = line_number;
        }
    }
}
