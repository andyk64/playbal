﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace PlayBAL
{
    class Emulator
    {
        public int CurrentLine { get; set; }

        // divisions where extra lines need to be added
        public List<string> AssemblyLines;
        public List<string> BytecodeLines;

        private Dictionary<int, int> bytecodeToLine;

        private List<string> assembly;

        public Emulator()
        {
            assembly = new List<string>();

            BytecodeLines = new List<string>();
        }

        public void Load(CodeDocument doc)
        {
            CPU.Bytecode = doc.Bytecode;
        }

        public List<byte> Build(string[] assembly_code) // not really "class emulator" job to do -- SWITCH TO CODEDOCUMENT SOON
        {
            var assembler = new Assembler();
            var bytes = assembler.Assemble(assembly_code);
            AssemblyLines = assembler.assemblyLines;
            BytecodeLines = assembler.bytecodeLines;
            return bytes;
        }

        public void Step()
        {
            CPU.Step();
        }
    }
}
