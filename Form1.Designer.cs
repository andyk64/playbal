﻿
namespace PlayBAL
{
    partial class PlayBAL
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PlayBAL));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnHexToggle = new System.Windows.Forms.Button();
            this.txtReg15 = new System.Windows.Forms.TextBox();
            this.txtReg14 = new System.Windows.Forms.TextBox();
            this.txtReg13 = new System.Windows.Forms.TextBox();
            this.txtReg12 = new System.Windows.Forms.TextBox();
            this.txtReg11 = new System.Windows.Forms.TextBox();
            this.txtReg10 = new System.Windows.Forms.TextBox();
            this.txtReg09 = new System.Windows.Forms.TextBox();
            this.txtReg08 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtReg07 = new System.Windows.Forms.TextBox();
            this.txtReg06 = new System.Windows.Forms.TextBox();
            this.txtReg05 = new System.Windows.Forms.TextBox();
            this.txtReg04 = new System.Windows.Forms.TextBox();
            this.txtReg03 = new System.Windows.Forms.TextBox();
            this.txtReg02 = new System.Windows.Forms.TextBox();
            this.txtReg01 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtReg00 = new System.Windows.Forms.TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menuFile = new System.Windows.Forms.ToolStripMenuItem();
            this.menuFileOpen = new System.Windows.Forms.ToolStripMenuItem();
            this.menuFileSave = new System.Windows.Forms.ToolStripMenuItem();
            this.menuFileSaveAs = new System.Windows.Forms.ToolStripMenuItem();
            this.menuFileExit = new System.Windows.Forms.ToolStripMenuItem();
            this.txtAssembly = new System.Windows.Forms.RichTextBox();
            this.btnBuild = new System.Windows.Forms.Button();
            this.btnPlay = new System.Windows.Forms.Button();
            this.btnStep = new System.Windows.Forms.Button();
            this.txtBytes = new System.Windows.Forms.RichTextBox();
            this.txtInput = new System.Windows.Forms.RichTextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtOutput = new System.Windows.Forms.RichTextBox();
            this.groupBox1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.btnHexToggle);
            this.groupBox1.Controls.Add(this.txtReg15);
            this.groupBox1.Controls.Add(this.txtReg14);
            this.groupBox1.Controls.Add(this.txtReg13);
            this.groupBox1.Controls.Add(this.txtReg12);
            this.groupBox1.Controls.Add(this.txtReg11);
            this.groupBox1.Controls.Add(this.txtReg10);
            this.groupBox1.Controls.Add(this.txtReg09);
            this.groupBox1.Controls.Add(this.txtReg08);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtReg07);
            this.groupBox1.Controls.Add(this.txtReg06);
            this.groupBox1.Controls.Add(this.txtReg05);
            this.groupBox1.Controls.Add(this.txtReg04);
            this.groupBox1.Controls.Add(this.txtReg03);
            this.groupBox1.Controls.Add(this.txtReg02);
            this.groupBox1.Controls.Add(this.txtReg01);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtReg00);
            this.groupBox1.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.groupBox1.Location = new System.Drawing.Point(974, 25);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(198, 265);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Registers";
            // 
            // btnHexToggle
            // 
            this.btnHexToggle.Location = new System.Drawing.Point(6, 236);
            this.btnHexToggle.Name = "btnHexToggle";
            this.btnHexToggle.Size = new System.Drawing.Size(186, 23);
            this.btnHexToggle.TabIndex = 33;
            this.btnHexToggle.Text = "hex / dec";
            this.btnHexToggle.UseVisualStyleBackColor = true;
            this.btnHexToggle.Click += new System.EventHandler(this.btnHexToggle_Click);
            // 
            // txtReg15
            // 
            this.txtReg15.Location = new System.Drawing.Point(128, 209);
            this.txtReg15.Name = "txtReg15";
            this.txtReg15.ReadOnly = true;
            this.txtReg15.Size = new System.Drawing.Size(62, 21);
            this.txtReg15.TabIndex = 32;
            this.txtReg15.Text = "00000000";
            this.txtReg15.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtReg14
            // 
            this.txtReg14.Location = new System.Drawing.Point(128, 182);
            this.txtReg14.Name = "txtReg14";
            this.txtReg14.ReadOnly = true;
            this.txtReg14.Size = new System.Drawing.Size(62, 21);
            this.txtReg14.TabIndex = 31;
            this.txtReg14.Text = "00000000";
            this.txtReg14.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtReg13
            // 
            this.txtReg13.Location = new System.Drawing.Point(128, 155);
            this.txtReg13.Name = "txtReg13";
            this.txtReg13.ReadOnly = true;
            this.txtReg13.Size = new System.Drawing.Size(62, 21);
            this.txtReg13.TabIndex = 30;
            this.txtReg13.Text = "00000000";
            this.txtReg13.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtReg12
            // 
            this.txtReg12.Location = new System.Drawing.Point(128, 128);
            this.txtReg12.Name = "txtReg12";
            this.txtReg12.ReadOnly = true;
            this.txtReg12.Size = new System.Drawing.Size(62, 21);
            this.txtReg12.TabIndex = 29;
            this.txtReg12.Text = "00000000";
            this.txtReg12.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtReg11
            // 
            this.txtReg11.Location = new System.Drawing.Point(128, 101);
            this.txtReg11.Name = "txtReg11";
            this.txtReg11.ReadOnly = true;
            this.txtReg11.Size = new System.Drawing.Size(62, 21);
            this.txtReg11.TabIndex = 28;
            this.txtReg11.Text = "00000000";
            this.txtReg11.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtReg10
            // 
            this.txtReg10.Location = new System.Drawing.Point(128, 74);
            this.txtReg10.Name = "txtReg10";
            this.txtReg10.ReadOnly = true;
            this.txtReg10.Size = new System.Drawing.Size(62, 21);
            this.txtReg10.TabIndex = 27;
            this.txtReg10.Text = "00000000";
            this.txtReg10.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtReg09
            // 
            this.txtReg09.Location = new System.Drawing.Point(128, 47);
            this.txtReg09.Name = "txtReg09";
            this.txtReg09.ReadOnly = true;
            this.txtReg09.Size = new System.Drawing.Size(62, 21);
            this.txtReg09.TabIndex = 26;
            this.txtReg09.Text = "00000000";
            this.txtReg09.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtReg08
            // 
            this.txtReg08.Location = new System.Drawing.Point(128, 20);
            this.txtReg08.Name = "txtReg08";
            this.txtReg08.ReadOnly = true;
            this.txtReg08.Size = new System.Drawing.Size(62, 21);
            this.txtReg08.TabIndex = 25;
            this.txtReg08.Text = "00000000";
            this.txtReg08.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(101, 213);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(21, 15);
            this.label9.TabIndex = 24;
            this.label9.Text = "15";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(101, 186);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(21, 15);
            this.label10.TabIndex = 23;
            this.label10.Text = "14";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(101, 159);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(21, 15);
            this.label11.TabIndex = 22;
            this.label11.Text = "13";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(101, 132);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(21, 15);
            this.label12.TabIndex = 21;
            this.label12.Text = "12";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(101, 105);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(21, 15);
            this.label13.TabIndex = 20;
            this.label13.Text = "11";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(101, 77);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(21, 15);
            this.label14.TabIndex = 19;
            this.label14.Text = "10";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(101, 50);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(21, 15);
            this.label15.TabIndex = 18;
            this.label15.Text = "09";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(101, 23);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(21, 15);
            this.label16.TabIndex = 17;
            this.label16.Text = "08";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(74, 213);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(21, 15);
            this.label5.TabIndex = 16;
            this.label5.Text = "07";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(74, 186);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(21, 15);
            this.label6.TabIndex = 15;
            this.label6.Text = "06";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(74, 159);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(21, 15);
            this.label7.TabIndex = 14;
            this.label7.Text = "05";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(74, 132);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(21, 15);
            this.label8.TabIndex = 13;
            this.label8.Text = "04";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(74, 105);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(21, 15);
            this.label3.TabIndex = 12;
            this.label3.Text = "03";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(74, 77);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(21, 15);
            this.label4.TabIndex = 11;
            this.label4.Text = "02";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(74, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 15);
            this.label1.TabIndex = 10;
            this.label1.Text = "01";
            // 
            // txtReg07
            // 
            this.txtReg07.Location = new System.Drawing.Point(6, 209);
            this.txtReg07.Name = "txtReg07";
            this.txtReg07.ReadOnly = true;
            this.txtReg07.Size = new System.Drawing.Size(62, 21);
            this.txtReg07.TabIndex = 9;
            this.txtReg07.Text = "00000000";
            this.txtReg07.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtReg06
            // 
            this.txtReg06.Location = new System.Drawing.Point(6, 182);
            this.txtReg06.Name = "txtReg06";
            this.txtReg06.ReadOnly = true;
            this.txtReg06.Size = new System.Drawing.Size(62, 21);
            this.txtReg06.TabIndex = 8;
            this.txtReg06.Text = "00000000";
            this.txtReg06.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtReg05
            // 
            this.txtReg05.Location = new System.Drawing.Point(6, 155);
            this.txtReg05.Name = "txtReg05";
            this.txtReg05.ReadOnly = true;
            this.txtReg05.Size = new System.Drawing.Size(62, 21);
            this.txtReg05.TabIndex = 7;
            this.txtReg05.Text = "00000000";
            this.txtReg05.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtReg04
            // 
            this.txtReg04.Location = new System.Drawing.Point(6, 128);
            this.txtReg04.Name = "txtReg04";
            this.txtReg04.ReadOnly = true;
            this.txtReg04.Size = new System.Drawing.Size(62, 21);
            this.txtReg04.TabIndex = 6;
            this.txtReg04.Text = "00000000";
            this.txtReg04.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtReg03
            // 
            this.txtReg03.Location = new System.Drawing.Point(6, 101);
            this.txtReg03.Name = "txtReg03";
            this.txtReg03.ReadOnly = true;
            this.txtReg03.Size = new System.Drawing.Size(62, 21);
            this.txtReg03.TabIndex = 5;
            this.txtReg03.Text = "00000000";
            this.txtReg03.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtReg02
            // 
            this.txtReg02.Location = new System.Drawing.Point(6, 74);
            this.txtReg02.Name = "txtReg02";
            this.txtReg02.ReadOnly = true;
            this.txtReg02.Size = new System.Drawing.Size(62, 21);
            this.txtReg02.TabIndex = 4;
            this.txtReg02.Text = "00000000";
            this.txtReg02.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtReg01
            // 
            this.txtReg01.Location = new System.Drawing.Point(6, 47);
            this.txtReg01.Name = "txtReg01";
            this.txtReg01.ReadOnly = true;
            this.txtReg01.Size = new System.Drawing.Size(62, 21);
            this.txtReg01.TabIndex = 3;
            this.txtReg01.Text = "00000000";
            this.txtReg01.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(74, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(21, 15);
            this.label2.TabIndex = 2;
            this.label2.Text = "00";
            // 
            // txtReg00
            // 
            this.txtReg00.Location = new System.Drawing.Point(6, 20);
            this.txtReg00.Name = "txtReg00";
            this.txtReg00.ReadOnly = true;
            this.txtReg00.Size = new System.Drawing.Size(62, 21);
            this.txtReg00.TabIndex = 1;
            this.txtReg00.Text = "00000000";
            this.txtReg00.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuFile});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1184, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // menuFile
            // 
            this.menuFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuFileOpen,
            this.menuFileSave,
            this.menuFileSaveAs,
            this.menuFileExit});
            this.menuFile.Name = "menuFile";
            this.menuFile.Size = new System.Drawing.Size(37, 20);
            this.menuFile.Text = "File";
            // 
            // menuFileOpen
            // 
            this.menuFileOpen.Name = "menuFileOpen";
            this.menuFileOpen.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.menuFileOpen.Size = new System.Drawing.Size(195, 22);
            this.menuFileOpen.Text = "Open";
            // 
            // menuFileSave
            // 
            this.menuFileSave.Name = "menuFileSave";
            this.menuFileSave.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.menuFileSave.Size = new System.Drawing.Size(195, 22);
            this.menuFileSave.Text = "Save";
            // 
            // menuFileSaveAs
            // 
            this.menuFileSaveAs.Name = "menuFileSaveAs";
            this.menuFileSaveAs.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.S)));
            this.menuFileSaveAs.Size = new System.Drawing.Size(195, 22);
            this.menuFileSaveAs.Text = "Save As...";
            // 
            // menuFileExit
            // 
            this.menuFileExit.Name = "menuFileExit";
            this.menuFileExit.Size = new System.Drawing.Size(195, 22);
            this.menuFileExit.Text = "Exit";
            // 
            // txtAssembly
            // 
            this.txtAssembly.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAssembly.HideSelection = false;
            this.txtAssembly.Location = new System.Drawing.Point(0, 25);
            this.txtAssembly.Name = "txtAssembly";
            this.txtAssembly.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.txtAssembly.Size = new System.Drawing.Size(777, 721);
            this.txtAssembly.TabIndex = 2;
            this.txtAssembly.Text = resources.GetString("txtAssembly.Text");
            this.txtAssembly.WordWrap = false;
            this.txtAssembly.SelectionChanged += new System.EventHandler(this.txtAssembly_SelectionChanged);
            this.txtAssembly.VScroll += new System.EventHandler(this.txtAssembly_VScroll);
            this.txtAssembly.TextChanged += new System.EventHandler(this.txtAssembly_TextChanged);
            // 
            // btnBuild
            // 
            this.btnBuild.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBuild.Location = new System.Drawing.Point(974, 296);
            this.btnBuild.Name = "btnBuild";
            this.btnBuild.Size = new System.Drawing.Size(62, 23);
            this.btnBuild.TabIndex = 3;
            this.btnBuild.Text = "Build";
            this.btnBuild.UseVisualStyleBackColor = true;
            this.btnBuild.Click += new System.EventHandler(this.btnBuild_Click);
            // 
            // btnPlay
            // 
            this.btnPlay.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPlay.Enabled = false;
            this.btnPlay.Location = new System.Drawing.Point(1042, 296);
            this.btnPlay.Name = "btnPlay";
            this.btnPlay.Size = new System.Drawing.Size(61, 23);
            this.btnPlay.TabIndex = 4;
            this.btnPlay.Text = "Play";
            this.btnPlay.UseVisualStyleBackColor = true;
            this.btnPlay.Click += new System.EventHandler(this.btnPlay_Click);
            // 
            // btnStep
            // 
            this.btnStep.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep.Enabled = false;
            this.btnStep.Location = new System.Drawing.Point(1110, 296);
            this.btnStep.Name = "btnStep";
            this.btnStep.Size = new System.Drawing.Size(62, 23);
            this.btnStep.TabIndex = 5;
            this.btnStep.Text = "Step";
            this.btnStep.UseVisualStyleBackColor = true;
            this.btnStep.Click += new System.EventHandler(this.btnStep_Click);
            // 
            // txtBytes
            // 
            this.txtBytes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBytes.HideSelection = false;
            this.txtBytes.Location = new System.Drawing.Point(775, 25);
            this.txtBytes.Name = "txtBytes";
            this.txtBytes.ReadOnly = true;
            this.txtBytes.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
            this.txtBytes.Size = new System.Drawing.Size(193, 721);
            this.txtBytes.TabIndex = 6;
            this.txtBytes.Text = resources.GetString("txtBytes.Text");
            // 
            // txtInput
            // 
            this.txtInput.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtInput.Location = new System.Drawing.Point(974, 749);
            this.txtInput.Name = "txtInput";
            this.txtInput.Size = new System.Drawing.Size(198, 100);
            this.txtInput.TabIndex = 7;
            this.txtInput.Text = resources.GetString("txtInput.Text");
            // 
            // label17
            // 
            this.label17.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(974, 732);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(98, 14);
            this.label17.TabIndex = 8;
            this.label17.Text = "Program Input";
            // 
            // txtOutput
            // 
            this.txtOutput.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtOutput.Location = new System.Drawing.Point(0, 749);
            this.txtOutput.Name = "txtOutput";
            this.txtOutput.Size = new System.Drawing.Size(968, 100);
            this.txtOutput.TabIndex = 9;
            this.txtOutput.Text = "";
            // 
            // PlayBAL
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1184, 861);
            this.Controls.Add(this.txtOutput);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.txtInput);
            this.Controls.Add(this.btnStep);
            this.Controls.Add(this.btnPlay);
            this.Controls.Add(this.btnBuild);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.txtBytes);
            this.Controls.Add(this.txtAssembly);
            this.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "PlayBAL";
            this.Text = "PlayBAL";
            this.Load += new System.EventHandler(this.PlayBAL_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtReg00;
        private System.Windows.Forms.TextBox txtReg07;
        private System.Windows.Forms.TextBox txtReg06;
        private System.Windows.Forms.TextBox txtReg05;
        private System.Windows.Forms.TextBox txtReg04;
        private System.Windows.Forms.TextBox txtReg03;
        private System.Windows.Forms.TextBox txtReg02;
        private System.Windows.Forms.TextBox txtReg01;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtReg15;
        private System.Windows.Forms.TextBox txtReg14;
        private System.Windows.Forms.TextBox txtReg13;
        private System.Windows.Forms.TextBox txtReg12;
        private System.Windows.Forms.TextBox txtReg11;
        private System.Windows.Forms.TextBox txtReg10;
        private System.Windows.Forms.TextBox txtReg09;
        private System.Windows.Forms.TextBox txtReg08;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menuFile;
        private System.Windows.Forms.ToolStripMenuItem menuFileOpen;
        private System.Windows.Forms.ToolStripMenuItem menuFileSave;
        private System.Windows.Forms.ToolStripMenuItem menuFileSaveAs;
        private System.Windows.Forms.ToolStripMenuItem menuFileExit;
        private System.Windows.Forms.RichTextBox txtAssembly;
        private System.Windows.Forms.Button btnBuild;
        private System.Windows.Forms.Button btnPlay;
        private System.Windows.Forms.Button btnStep;
        private System.Windows.Forms.RichTextBox txtBytes;
        private System.Windows.Forms.Button btnHexToggle;
        private System.Windows.Forms.RichTextBox txtInput;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.RichTextBox txtOutput;
    }
}

