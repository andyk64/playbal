﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace PlayBAL
{
    public partial class PlayBAL : Form
    {
        private readonly string title = "PlayBAL";
        private bool assemblyTextChanged;
        private string assemblyFileName;
        private string assemblyText;
        private string oldBytes; // WE WILL REMOVE THIS LATER DO NOT FORGET

        private TextBox[] Registers;

        private bool displayHex = true;

        private CodeDocument codeDoc = null;

        public PlayBAL()
        {
            InitializeComponent();

            oldBytes = txtBytes.Text; // REMOVE THIS LATER

            RefreshRegisters();
            for (int i = txtBytes.Lines.Length; i <= txtAssembly.Lines.Length; ++i) txtBytes.Text += "\n"; // match bytecode text length to assembly text length
        }

        private void PlayBAL_Load(object sender, EventArgs e)
        {
            assemblyTextChanged = false;
            assemblyFileName = string.Empty;

            Text = title;

            menuFileOpen.Click += MenuFileOpen;
            menuFileSave.Click += MenuFileSave;
            menuFileSaveAs.Click += MenuFileSaveAs;
            menuFileExit.Click += MenuFileExit;
        }

        private void MenuFileOpen(object sender, EventArgs e)
        {
            OpenFileDialog openFile = new()
            {
                Filter = "All files (*.*)|*.*"
            };

            if (openFile.ShowDialog() == DialogResult.OK)
            {
                assemblyFileName = openFile.FileName;
                Text = title + " - " + assemblyFileName;
                txtAssembly.Text = System.IO.File.ReadAllText(assemblyFileName);
                assemblyTextChanged = false;
            }
        }

        private void MenuFileSave(object sender, EventArgs e)
        {
            if (assemblyFileName != string.Empty)
            {
                Text = title + " - " + assemblyFileName;
                System.IO.File.WriteAllText(assemblyFileName, txtAssembly.Text);
                assemblyTextChanged = false;
            }
            else
            {
                menuFileSaveAs.PerformClick();
            }
        }

        private void MenuFileSaveAs(object sender, EventArgs e)
        {
            SaveFileDialog saveFile = new()
            {
                Filter = "All files (*.*)|*.*"
            };

            if (saveFile.ShowDialog() == DialogResult.OK)
            {
                assemblyFileName = saveFile.FileName;
                Text = title + " - " + assemblyFileName;
                System.IO.File.WriteAllText(assemblyFileName, txtAssembly.Text);
                assemblyTextChanged = false;
            }
        }

        private void MenuFileExit(object sender, EventArgs e)
        {
            if (assemblyTextChanged)
            {
                if (MessageBox.Show("Assembly file has changed, save before exit?", title, MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    menuFileSave.PerformClick();
                }
            }

            Close();
        }

        private void txtAssembly_TextChanged(object sender, EventArgs e)
        {
            assemblyTextChanged = true;
        }

        private void btnBuild_Click(object sender, EventArgs e)
        {
            int scroll = txtAssembly.GetLineFromCharIndex(txtAssembly.GetCharIndexFromPosition(new Point(3, 3)));
            if (!txtAssembly.ReadOnly) // DO BUILD
            {
                Emulator emu = new();

                assemblyText = txtAssembly.Text;
                txtAssembly.ReadOnly = true;
                txtInput.ReadOnly = true;
                try
                {
                    List<byte> bytecode = emu.Build(txtAssembly.Lines);

                    codeDoc = new CodeDocument(txtAssembly.Lines);
                    codeDoc.Build();
                    CPU.Load(codeDoc.Bytecode, txtInput.Text);

                    txtAssembly.Text = string.Join('\n', codeDoc.AssembledLines);
                    txtBytes.Text = string.Join('\n', codeDoc.BytecodeLines);

                    // PREPARE FOR STEPPING THROUGH CODE
                    var lineNum = codeDoc.GetLineFromByteIndex(CPU.Head);
                    System.Diagnostics.Debug.WriteLine(lineNum);
                    HighlightLineNumber(txtAssembly, lineNum);
                    HighlightLineNumber(txtBytes, lineNum);

                    txtAssembly.ReadOnly = true;
                    btnPlay.Enabled = true;
                    btnStep.Enabled = true;
                    btnBuild.Text = "Edit";

                    //txtAssembly.SelectionStart = txtAssembly.GetFirstCharIndexFromLine(scroll);
                    //txtAssembly.ScrollToCaret();
                }
                catch (AssemblerException ex)
                {
                    var from = txtAssembly.GetFirstCharIndexFromLine(ex.LineNumber);
                    var to = txtAssembly.GetFirstCharIndexFromLine(ex.LineNumber + 1);

                    txtAssembly.SelectionStart = from;
                    txtAssembly.SelectionLength = to - from;
                    txtAssembly.SelectionBackColor = Color.Red;
                    txtAssembly.ScrollToCaret();
                    txtAssembly.ReadOnly = false;
                    
                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK);
                }
            }
            else // RETURN TO EDIT
            {
                CPU.Reset(); // RESET CPU ! ! ! ! ! ! ! !
                RefreshRegisters();

                txtAssembly.Text = assemblyText;

                txtBytes.Text = oldBytes; // REMOVE THIS LATER

                txtAssembly.ReadOnly = false;
                txtInput.ReadOnly = false;
                btnPlay.Enabled = false;
                btnStep.Enabled = false;
                btnBuild.Text = "Build";

                txtAssembly.SelectionStart = txtAssembly.GetFirstCharIndexFromLine(scroll);
                txtAssembly.ScrollToCaret();
            }
        }

        private void txtAssembly_VScroll(object sender, EventArgs e)
        {
            var index = txtAssembly.GetCharIndexFromPosition(new Point(1, 12));
            var line = txtAssembly.GetLineFromCharIndex(index);
            var pos = txtBytes.GetFirstCharIndexFromLine(line);

            txtBytes.SelectionStart = pos;
            txtBytes.SelectionLength = 0;
            txtBytes.ScrollToCaret();
        }

        private void RefreshRegisters()
        {
            /* set up registers textbox array */
            if (Registers == null)
            {
                Registers = new TextBox[16];
                Registers[00] = txtReg00;
                Registers[01] = txtReg01;
                Registers[02] = txtReg02;
                Registers[03] = txtReg03;
                Registers[04] = txtReg04;
                Registers[05] = txtReg05;
                Registers[06] = txtReg06;
                Registers[07] = txtReg07;
                Registers[08] = txtReg08;
                Registers[09] = txtReg09;
                Registers[10] = txtReg10;
                Registers[11] = txtReg11;
                Registers[12] = txtReg12;
                Registers[13] = txtReg13;
                Registers[14] = txtReg14;
                Registers[15] = txtReg15;
            }

            for (int i = 0; i < 16; ++i)
            {
                if (displayHex)
                    Registers[i].Text = CPU.Registers[i].ToString("X8");
                else
                    Registers[i].Text = CPU.Registers[i].ToString();
            }
        }

        private void btnHexToggle_Click(object sender, EventArgs e)
        {
            displayHex = !displayHex;

            RefreshRegisters();
        }

        private void txtAssembly_SelectionChanged(object sender, EventArgs e)
        {
            var lineNum = txtAssembly.GetLineFromCharIndex(txtAssembly.SelectionStart);
            if (lineNum >= txtBytes.Lines.Length) return;
            var target = txtBytes.GetFirstCharIndexFromLine(lineNum);

            txtBytes.SelectionStart = target;
            txtBytes.SelectionLength = txtBytes.Lines[lineNum].Length;
        }

        private void btnPlay_Click(object sender, EventArgs e)
        {
            while (!CPU.Finished)
            {
                CPU.Step();

                if (CPU.Finished)
                {
                    System.Diagnostics.Debug.WriteLine("Program finished!");
                    return;
                }

                RefreshRegisters();
                txtBytes.Text = codeDoc.GetBytecodeLines();
                txtOutput.Text = CPU.OutputText;

                var lineNum = codeDoc.GetLineFromByteIndex(CPU.Head);
                HighlightLineNumber(txtAssembly, lineNum);
                HighlightLineNumber(txtBytes, lineNum);
            }
        }

        private void HighlightLineNumber(RichTextBox target, int lineNumber)
        {
            target.SelectionStart = target.GetFirstCharIndexFromLine(lineNumber);
            target.SelectionLength = target.Lines[lineNumber].Length;
        }

        private void btnStep_Click(object sender, EventArgs e)
        {
            CPU.Step();

            if (CPU.Finished)
            {
                System.Diagnostics.Debug.WriteLine("Program finished!");
                return;
            }

            RefreshRegisters();
            txtBytes.Text = codeDoc.GetBytecodeLines();
            txtOutput.Text = CPU.OutputText;

            var lineNum = codeDoc.GetLineFromByteIndex(CPU.Head);
            HighlightLineNumber(txtAssembly, lineNum);
            HighlightLineNumber(txtBytes, lineNum);
        }
    }
}
