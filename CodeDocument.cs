﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PlayBAL
{
    public class CodeDocument
    {
        public List<string> OriginalLines { get; set; }
        public List<string> AssembledLines { get; set; }
        public List<string> BytecodeLines { get; set; }
        public List<byte> Bytecode { get; set; }
        private Dictionary<int, int> ByteLinePairs { get; set; } = new(); // maybe make private

        public CodeDocument(string[] code)
        {
            OriginalLines = new(code);
        }

        public bool Build()
        {
            // ON ERROR LOOK INTO THROWING MULTIPLE ERRORED EXCEPTION

            var assembler = new Assembler();
            var bytes = assembler.Assemble(OriginalLines.ToArray());

            AssembledLines = assembler.assemblyLines;
            BytecodeLines = assembler.bytecodeLines;
            Bytecode = bytes;

            for (int lineNum = 0, currentBytePos = 0; lineNum < BytecodeLines.Count; ++lineNum)
            {
                var count = System.Text.RegularExpressions.Regex.Replace(BytecodeLines[lineNum], @"\W*", string.Empty).Length / 2;

                if (count > 0)
                {
                    //System.Diagnostics.Debug.WriteLine(lineNum + ": " + currentBytePos);
                    ByteLinePairs[currentBytePos] = lineNum;
                    currentBytePos += count;
                }
            }

            return true;
        }

        public string GetBytecodeLines()
        {
            string text = string.Empty;
            int lineCount = 0;
            int bytesSinceNewline = 0;
            
            for (int i = 0; i < Bytecode.Count; ++i)
            {
                if (ByteLinePairs.ContainsKey(i))
                {
                    for (int j = lineCount; j < ByteLinePairs[i]; ++j)
                    {
                        text += Environment.NewLine;
                        lineCount++;
                        bytesSinceNewline = 0;
                    }

                    text += i.ToString("X4") + ": ";
                }

                text += Bytecode[i].ToString("X2");
                bytesSinceNewline++;

                if (bytesSinceNewline % 2 == 0) text += " ";
            }

            for (int i = 0; i < AssembledLines.Count - lineCount; ++i) text += Environment.NewLine;

            return text;
        }
        
        public int GetLineFromByteIndex(int index)
        {
            int closestByte = -1;
            int closestLine = 0;

            foreach (var pair in ByteLinePairs)
            {
                if (Math.Abs(pair.Key - index) < Math.Abs(closestByte - index))
                {
                    closestByte = pair.Key;
                    closestLine = pair.Value;
                }
            }

            return closestLine;
        }
    }
}
