﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlayBAL
{
    public static class EBCDIC
    {
        public static char[] charset;

        static EBCDIC()
        {
            charset[0x0] = ' ';
            charset[0x1] = ' ';
            charset[0x2] = ' ';
            charset[0x3] = ' ';
            charset[0x4] = ' ';
            charset[0x5] = ' ';
            charset[0x6] = ' ';
            charset[0x7] = ' ';
            charset[0x8] = ' ';
            charset[0x9] = ' ';
            charset[0xa] = ' ';
            charset[0xb] = ' ';
            charset[0xc] = ' ';
            charset[0xd] = ' ';
            charset[0xe] = ' ';
            charset[0xf] = ' ';
            charset[0x10] = ' ';
            charset[0x11] = ' ';
            charset[0x12] = ' ';
            charset[0x13] = ' ';
            charset[0x14] = ' ';
            charset[0x15] = ' ';
            charset[0x16] = ' ';
            charset[0x17] = ' ';
            charset[0x18] = ' ';
            charset[0x19] = ' ';
            charset[0x1a] = ' ';
            charset[0x1b] = ' ';
            charset[0x1c] = ' ';
            charset[0x1d] = ' ';
            charset[0x1e] = ' ';
            charset[0x1f] = ' ';
            charset[0x20] = ' ';
            charset[0x21] = ' ';
            charset[0x22] = ' ';
            charset[0x23] = ' ';
            charset[0x24] = ' ';
            charset[0x25] = ' ';
            charset[0x26] = ' ';
            charset[0x27] = ' ';
            charset[0x28] = ' ';
            charset[0x29] = ' ';
            charset[0x2a] = ' ';
            charset[0x2b] = ' ';
            charset[0x2c] = ' ';
            charset[0x2d] = ' ';
            charset[0x2e] = ' ';
            charset[0x2f] = ' ';
            charset[0x30] = ' ';
            charset[0x31] = ' ';
            charset[0x32] = ' ';
            charset[0x33] = ' ';
            charset[0x34] = ' ';
            charset[0x35] = ' ';
            charset[0x36] = ' ';
            charset[0x37] = ' ';
            charset[0x38] = ' ';
            charset[0x39] = ' ';
            charset[0x3a] = ' ';
            charset[0x3b] = ' ';
            charset[0x3c] = ' ';
            charset[0x3d] = ' ';
            charset[0x3e] = ' ';
            charset[0x3f] = ' ';
            charset[0x40] = ' '; // REAL SPACE
            charset[0x41] = ' ';
            charset[0x42] = ' ';
            charset[0x43] = ' ';
            charset[0x44] = ' ';
            charset[0x45] = ' ';
            charset[0x46] = ' ';
            charset[0x47] = ' ';
            charset[0x48] = ' ';
            charset[0x49] = ' ';
            charset[0x4a] = ' ';
            charset[0x4b] = '.';
            charset[0x4c] = ' ';
            charset[0x4d] = '(';
            charset[0x4e] = '+';
            charset[0x4f] = ' ';
            charset[0x50] = '&';
            charset[0x51] = ' ';
            charset[0x52] = ' ';
            charset[0x53] = ' ';
            charset[0x54] = ' ';
            charset[0x55] = ' ';
            charset[0x56] = ' ';
            charset[0x57] = ' ';
            charset[0x58] = ' ';
            charset[0x59] = ' ';
            charset[0x5a] = ' ';
            charset[0x5b] = '$';
            charset[0x5c] = '*';
            charset[0x5d] = ')';
            charset[0x5e] = ' ';
            charset[0x5f] = ' ';
            charset[0x60] = '-';
            charset[0x61] = '/';
            charset[0x62] = ' ';
            charset[0x63] = ' ';
            charset[0x64] = ' ';
            charset[0x65] = ' ';
            charset[0x66] = ' ';
            charset[0x67] = ' ';
            charset[0x68] = ' ';
            charset[0x69] = ' ';
            charset[0x6a] = ' ';
            charset[0x6b] = ',';
            charset[0x6c] = ' ';
            charset[0x6d] = '_';
            charset[0x6e] = ' ';
            charset[0x6f] = ' ';
            charset[0x70] = ' ';
            charset[0x71] = ' ';
            charset[0x72] = ' ';
            charset[0x73] = ' ';
            charset[0x74] = ' ';
            charset[0x75] = ' ';
            charset[0x76] = ' ';
            charset[0x77] = ' ';
            charset[0x78] = ' ';
            charset[0x79] = ' ';
            charset[0x7a] = ' ';
            charset[0x7b] = '#';
            charset[0x7c] = '@';
            charset[0x7d] = '\'';
            charset[0x7e] = '=';
            charset[0x7f] = ' ';
            charset[0x80] = ' ';
            charset[0x81] = 'a';
            charset[0x82] = 'b';
            charset[0x83] = 'c';
            charset[0x84] = 'd';
            charset[0x85] = 'e';
            charset[0x86] = 'f';
            charset[0x87] = 'g';
            charset[0x88] = 'h';
            charset[0x89] = 'i';
            charset[0x8a] = ' ';
            charset[0x8b] = ' ';
            charset[0x8c] = ' ';
            charset[0x8d] = ' ';
            charset[0x8e] = ' ';
            charset[0x8f] = ' ';
            charset[0x90] = ' ';
            charset[0x91] = 'j';
            charset[0x92] = 'k';
            charset[0x93] = 'l';
            charset[0x94] = 'm';
            charset[0x95] = 'n';
            charset[0x96] = 'o';
            charset[0x97] = 'p';
            charset[0x98] = 'q';
            charset[0x99] = 'r';
            charset[0x9a] = ' ';
            charset[0x9b] = ' ';
            charset[0x9c] = ' ';
            charset[0x9d] = ' ';
            charset[0x9e] = ' ';
            charset[0x9f] = ' ';
            charset[0xa0] = ' ';
            charset[0xa1] = ' ';
            charset[0xa2] = 's';
            charset[0xa3] = 't';
            charset[0xa4] = 'u';
            charset[0xa5] = 'v';
            charset[0xa6] = 'w';
            charset[0xa7] = 'x';
            charset[0xa8] = 'y';
            charset[0xa9] = 'z';
            charset[0xaa] = ' ';
            charset[0xab] = ' ';
            charset[0xac] = ' ';
            charset[0xad] = ' ';
            charset[0xae] = ' ';
            charset[0xaf] = ' ';
            charset[0xb0] = ' ';
            charset[0xb1] = ' ';
            charset[0xb2] = ' ';
            charset[0xb3] = ' ';
            charset[0xb4] = ' ';
            charset[0xb5] = ' ';
            charset[0xb6] = ' ';
            charset[0xb7] = ' ';
            charset[0xb8] = ' ';
            charset[0xb9] = ' ';
            charset[0xba] = ' ';
            charset[0xbb] = ' ';
            charset[0xbc] = ' ';
            charset[0xbd] = ' ';
            charset[0xbe] = ' ';
            charset[0xbf] = ' ';
            charset[0xc0] = ' ';
            charset[0xc1] = 'A';
            charset[0xc2] = 'B';
            charset[0xc3] = 'C';
            charset[0xc4] = 'D';
            charset[0xc5] = 'E';
            charset[0xc6] = 'F';
            charset[0xc7] = 'G';
            charset[0xc8] = 'H';
            charset[0xc9] = 'I';
            charset[0xca] = ' ';
            charset[0xcb] = ' ';
            charset[0xcc] = ' ';
            charset[0xcd] = ' ';
            charset[0xce] = ' ';
            charset[0xcf] = ' ';
            charset[0xd0] = ' ';
            charset[0xd1] = 'J';
            charset[0xd2] = 'K';
            charset[0xd3] = 'L';
            charset[0xd4] = 'M';
            charset[0xd5] = 'N';
            charset[0xd6] = 'O';
            charset[0xd7] = 'P';
            charset[0xd8] = 'Q';
            charset[0xd9] = 'R';
            charset[0xda] = ' ';
            charset[0xdb] = ' ';
            charset[0xdc] = ' ';
            charset[0xdd] = ' ';
            charset[0xde] = ' ';
            charset[0xdf] = ' ';
            charset[0xe0] = ' ';
            charset[0xe1] = ' ';
            charset[0xe2] = 'S';
            charset[0xe3] = 'T';
            charset[0xe4] = 'U';
            charset[0xe5] = 'V';
            charset[0xe6] = 'W';
            charset[0xe7] = 'X';
            charset[0xe8] = 'Y';
            charset[0xe9] = 'Z';
            charset[0xea] = ' ';
            charset[0xeb] = ' ';
            charset[0xec] = ' ';
            charset[0xed] = ' ';
            charset[0xee] = ' ';
            charset[0xef] = ' ';
            charset[0xf0] = '0';
            charset[0xf1] = '1';
            charset[0xf2] = '2';
            charset[0xf3] = '3';
            charset[0xf4] = '4';
            charset[0xf5] = '5';
            charset[0xf6] = '6';
            charset[0xf7] = '7';
            charset[0xf8] = '8';
            charset[0xf9] = '9';
            charset[0xfa] = ' ';
            charset[0xfb] = ' ';
            charset[0xfc] = ' ';
            charset[0xfd] = ' ';
            charset[0xfe] = ' ';
            charset[0xff] = ' ';
        }
    }
}
